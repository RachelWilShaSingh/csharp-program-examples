﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AddCustomerWorkFlowProject
{
    public class Customer
    {
        #region customer attributes
        public string uniquekey { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        #endregion

        #region List Intialization

        public Customer(string firstname, string lastname, string phone, string email)
        {
            this.firstname = firstname;
            this.lastname = lastname;
            this.uniquekey = firstname+lastname+email;
            this.phone = phone;
            this.email = email;
        }



        #endregion
        #region Actions

        public void DisplayCustomer()
        {
            Console.Write(this.firstname + " " + this.lastname);
            Console.Write("\t");
            Console.Write(this.phone);
            Console.Write("\t");
            Console.Write(this.email);
            Console.Write("\t");
            Console.Write("Unique key: " + this.uniquekey);
            Console.WriteLine();
        }

        #endregion

    }


}
