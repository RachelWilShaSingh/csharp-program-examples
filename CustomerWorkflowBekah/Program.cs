﻿using System;
using NUnit.Framework;

namespace AddCustomerWorkFlowProject
{
    class Program
    {
		public static void AddCustomer(CustomerList customerlist)
		{
			Console.WriteLine("------------------------------------------------");
			Console.WriteLine("ADD CUSTOMER");
			Console.WriteLine("------------------------------------------------");
			Console.WriteLine("Please enter the customer's first name and last name to continue");
			Console.WriteLine("Please enter customers first name");
			string firstname = Console.ReadLine();
			Console.WriteLine("Please enter the customers last name");
			string lastname = Console.ReadLine();
			Console.WriteLine("Please enter the additional information for this customer");
			bool phonevalid = false;
			string phone = "8009807488";
			while (phonevalid != true)
			{
				Console.WriteLine("Phone Number");
				phone = Console.ReadLine();
				if (customerlist.CheckPhoneValidity(phone) == true)
				{
					phonevalid = true;
				}
			}
			string email = "placeholder";
			bool emailvalid = false;
			while (emailvalid != true)
			{
				Console.WriteLine("Please enter a valid email");
				email = Console.ReadLine();
				emailvalid = customerlist.CheckEmailValidity(email);
			}
			string fullname = firstname + lastname + email;
			if (customerlist.CustomerAlreadyExists(fullname) == true)
			{
				Console.WriteLine("This customer appears to already exist and cannot be added at this time");
				return; // End creating new customer early
			}
			else
			{
				Console.WriteLine("Please confirm you wish to add this customer: y/n");
				string choice = Console.ReadLine();
				if (choice == "y")
				{
					customerlist.AddCustomer(firstname, lastname, phone, email);
					Console.WriteLine("you have added customer: ");
					customerlist.DisplaySingleCustomer();
				}
			}
		}
		
		public static void ViewCustomers(CustomerList customerlist)
		{
			Console.WriteLine("------------------------------------------------");
			Console.WriteLine("VIEW CUSTOMERS");
			Console.WriteLine("------------------------------------------------");
			customerlist.DisplayListofCustomers();
		}
		
		public static void AddLead(CustomerList customerlist)
		{
			Console.WriteLine("------------------------------------------------");
			Console.WriteLine("ADD LEAD");
			Console.WriteLine("------------------------------------------------");
			// Which customer are we adding the lead to?
			Console.WriteLine("ADD LEAD");
			Console.WriteLine("Customers:");
			customerlist.DisplayListofCustomers();
			Console.WriteLine("Enter customer key: ");

			string customerKey = Console.ReadLine();

			Customer editMe;

			try
			{
				editMe = customerlist.GetCustomerWithKey(customerKey);
			}
			catch(ArgumentException ex)
            {
				Console.WriteLine(ex.Message);
				return;	// Exit early; can't add lead to nonexistant person.
            }

			Console.WriteLine("\n");
			Console.WriteLine("EDITING:");
			editMe.DisplayCustomer();

			// Add lead to customer
		}
		
		public static void UpdateLead(CustomerList customerlist)
		{
			Console.WriteLine("------------------------------------------------");
			Console.WriteLine("UPDATE LEAD");
			Console.WriteLine("------------------------------------------------");
		}

		public static void PreEnterCustomers(CustomerList customerlist)
		{
			customerlist.AddCustomer("An", "Fo", "1234567890", "a@zz.com");
			customerlist.AddCustomer("Bo", "Ge", "1234567891", "b@zz.com");
			customerlist.AddCustomer("Go", "As", "1234567892", "c@zz.com");
			customerlist.AddCustomer("Wo", "Be", "1234567893", "d@zz.com");
		}
		
        public static void Main()
        {
            CustomerList customerlist = new CustomerList();
            bool done = false;

			PreEnterCustomers(customerlist);

			while ( done != true )
            {
				Console.WriteLine("\n\n");
				Console.WriteLine( "------------------------------------------------" );
				Console.WriteLine( "MAIN MENU" );
				Console.WriteLine("------------------------------------------------");
				Console.WriteLine( "1. Add customer" );
				Console.WriteLine( "2. View all customers" );
				Console.WriteLine( "3. Add lead to customer" );
				Console.WriteLine( "4. Update lead status" );
				Console.WriteLine( "0. Quit" );
				Console.WriteLine( "" );
				
				int choice = Convert.ToInt32( Console.ReadLine() );
				
				if ( choice == 1 )
				{
					AddCustomer(customerlist);
				}
				else if ( choice == 2 )
				{
					ViewCustomers(customerlist);
				}
				else if ( choice == 3 )
				{
					AddLead(customerlist);
				}
				else if ( choice == 4 )
				{
					UpdateLead(customerlist);
				}
				else if ( choice == 0 )
				{
					done = true;
				}
			}

        }
    }
}
