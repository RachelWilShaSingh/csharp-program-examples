﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AddCustomerWorkFlowProject
{
    public class CustomerList
    {

        List<Customer> customerlist = new List<Customer>();


        public void AddCustomer(string firstname, string lastname, string phone, string email)
        {

            Console.WriteLine("The customer has been added to the database");
            customerlist.Add(new Customer(firstname, lastname, phone, email));
        }

        public bool CustomerAlreadyExists(string fullname)
        {
            if (customerlist.Exists(element => element.uniquekey == fullname))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Customer GetCustomerWithKey(string key)
        {
            // Return whichever customer has this key
            // Error: If key doesn't match anything in the list
            var result = customerlist.SingleOrDefault(el => el.uniquekey == key);

            if ( result == null )
            {
                throw new ArgumentException("There is no customer with unique key " + key);
            }

            return result;
        }

        public void DisplayListofCustomers()
        {
            foreach (Customer customer in customerlist)
            {
                customer.DisplayCustomer();
                Console.WriteLine("\n");
            }
        }

        public void DisplaySingleCustomer()
        {
            int customertodisplay = customerlist.Count -1;
            customerlist[customertodisplay].DisplayCustomer();
        }

        public bool CheckPhoneValidity(string phone)
        {
            if (phone.Length != 10)
            {
                Console.WriteLine(new ArgumentException("Phone number must be 10 digits", nameof(phone)));
                return false;
            }
            else if (phone == null)
            {
                Console.WriteLine(new ArgumentNullException("Phone Number Cannot be null"));
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool CheckEmailValidity(string email)
        {
            // code below from https://code.4noobz.net/c-email-validation/
            var regex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            bool isValid = Regex.IsMatch(email, regex, RegexOptions.IgnoreCase);
            return isValid;
            //if (email.Contains("@") )
            //{
            //    if (email.Contains(".com"))
            //    {
            //        return true;
            //    }
            //    else if (email.Contains(".net"))
            //    {
            //        return true;
            //    }
            //    else if (email.Contains(".org"))
            //    {
            //        return true;
            //    }
            //    else if (email.Contains(".edu"))
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        Console.WriteLine(new ArgumentException("Please enter a valid email"));
            //        return false;
            //    }
            //}
            //else
            //{
            //    Console.WriteLine(new ArgumentException("Please enter a valid email"));
            //    return false; 
            //}
        }
    }
}
